import React, { useEffect, useState } from 'react'

import './App.css'
import add from './add.svg'
import bookmark from './bookmark.svg'
import bookmarked from './bookmarked.svg'
import camera from './camera.svg'
import dm from './dm.svg'
import heart from './heart.svg'
import logo from './logo.png'
import home from './home.svg'
import homeActive from './homeActive.svg'
import Post from './Post'
import search from './search.svg'

let apiData = []

function App() {
	useEffect(() => {
		fetch(
			'https://hiit.ria.rocks/videos_api/cdn/com.rstream.crafts?versionCode=40&lurl=Canvas%20painting%20ideas'
		)
			.then(res => res.json())
			.then(result => {
				apiData = result
				setData(apiData)
				setLoading(false)
			})
	}, [])

	const [data, setData] = useState(apiData)

	const [loading, setLoading] = useState(true)

	const [page, setPage] = useState(false)

	const navigate = value => {
		if (value)
			setData(apiData.filter(post => localStorage.getItem(post.id) === 'true'))
		else setData(apiData)
		setPage(value)
	}

	const toggleBookMark = (id, value) => localStorage.setItem(id, value)

	return (
		<>
			<div className='App'>
				<header className='AppHeader'>
					<div className='IconSmall CameraIcon'>
						<img src={camera} className='icon' alt='camera' />
					</div>
					<div className='IconLarge AppLogo'>
						<img src={logo} className='icon' alt='logo' />
					</div>
					<div className='IconSmall DMIcon'>
						<img src={dm} className='icon' alt='direct message' />
					</div>
				</header>
				<main>
					{data &&
						data.map((post, index) => (
							<Post
								post={post}
								index={index}
								key={index}
								toggleBookMark={toggleBookMark}
							/>
						))}
				</main>
				<footer className='AppFooter'>
					<img
						src={page ? home : homeActive}
						onClick={() => navigate(false)}
						className='icon'
						alt='home page'
					/>
					<img src={search} className='icon' alt='search page' />
					<img src={add} className='icon' alt='add post' />
					<img src={heart} className='icon' alt='favorites' />
					<img
						src={page ? bookmarked : bookmark}
						className='icon'
						onClick={() => navigate(true)}
						alt='bookmarks'
					/>
				</footer>
			</div>
			{loading && <div className='LoadingScreen'>Loading...</div>}
		</>
	)
}

export default App
