import React, { useState } from 'react'

import bookmarkIcon from './bookmark.svg'
import bookmarked from './bookmarked.svg'
import comment from './comment.svg'
import dm from './dm.svg'
import heart from './heart.svg'
import moreIcon from './more.svg'

const fakeComments = [
	{ username: 'James', comments: 'not so cool' },
	{ username: 'James', comments: 'awesome' },
	{ username: 'James', comments: 'awesome' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'awesome' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'This looks sad' },
	{ username: 'James', comments: 'not so cool' },
	{ username: 'James', comments: 'not so cool' },
	{ username: 'James', comments: 'not so cool' },
	{ username: 'James', comments: 'not so cool' },
	{ username: 'James', comments: 'awesome' },
]

const Post = ({ post, index, toggleBookMark }) => {
	const [bookmark, setBookmark] = useState(
		localStorage.getItem(post.id) === 'true' ? true : false
	)

	const [comments, setComments] = useState(false)

	const [more, setMore] = useState(false)

	const addBookmark = () => {
		toggleBookMark(post.id, !bookmark)
		setBookmark(!bookmark)
	}

	return (
		<div className='Post'>
			<div className='PostHeader'>
				<div
					className='PostAvatar'
					style={{
						backgroundImage: 'url(' + post['low thumbnail'] + ')',
					}}
				></div>
				<div className='PostProfileName'>{post.id}</div>
				<img src={moreIcon} className='icon MoreIcon' alt='more options' />
			</div>
			<div
				className='PostImage'
				style={{
					backgroundImage: 'url(' + post['high thumbnail'] + ')',
				}}
			></div>
			<div className='PostActions'>
				<img src={heart} className='icon' alt='like' />
				<img src={comment} className='icon' alt='comment' />
				<img src={dm} className='icon' alt='share' />
				<img
					src={bookmark ? bookmarked : bookmarkIcon}
					className='icon BookmarkIcon'
					onClick={addBookmark}
					alt='bookmark post'
				/>
			</div>
			<div className='PostLikes'>
				<div className='PostFakeLikers'></div>
				<div className='PostFakeLikers'></div>
				<div className='PostFakeLikers'></div>
				<p>Liked by so many people</p>
			</div>
			<div className='PostTitle'>
				<p>
					<b>{post.id} </b>
					{post.title.length > 50 && !more ? (
						<>
							{post.title.slice(0, 50)}...
							<a className='textSecondaryColor' onClick={() => setMore(true)}>
								more
							</a>
						</>
					) : (
						post.title
					)}
				</p>
			</div>
			<div className='PostComments'>
				{comments && (
					<>
						<p>
							<b>{fakeComments[index % fakeComments.length].username} </b>
							{fakeComments[index % fakeComments.length].comments}
						</p>
						<p>
							<b>{fakeComments[(index + 1) % fakeComments.length].username} </b>
							{fakeComments[(index + 1) % fakeComments.length].comments}
						</p>
					</>
				)}
				{!comments && (
					<p className='textSecondaryColor' onClick={() => setComments(true)}>
						View all 2 comments
					</p>
				)}
			</div>
		</div>
	)
}

export default Post
